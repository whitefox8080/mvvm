﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MVVM
{
    public class ApplicationViewModel: INotifyPropertyChanged
    {
        private Phone _selectedPhone;
        public ObservableCollection<Phone> Phones { get; set; }

        IFileService _fileService;
        IDialogService _dialogService;

        // save file
        private RelayCommand _saveCommand;
        public RelayCommand SaveCommand
        {
            get
            {
                return _saveCommand ??
                       (_saveCommand = new RelayCommand(obj =>
                       {
                           try
                           {
                               if (_dialogService.SaveFileDialog() != true) return;
                               _fileService.Save(_dialogService.FilePath, Phones.ToList());
                               _dialogService.ShowMessage("Файл сохранен");
                           }
                           catch (Exception ex)
                           {
                               _dialogService.ShowMessage(ex.Message);
                           }
                       }));
            }
        }

        // open files
        private RelayCommand _openCommand;
        public RelayCommand OpenCommand
        {
            get
            {
                return _openCommand ??
                       (_openCommand = new RelayCommand(obj =>
                       {
                           try
                           {
                               if (_dialogService.OpenFileDialog() != true) return;
                               var phones = _fileService.Open(_dialogService.FilePath);
                               Phones.Clear();
                               foreach (var p in phones)
                                   Phones.Add(p);
                               _dialogService.ShowMessage("Файл открыт");
                           }
                           catch (Exception ex)
                           {
                               _dialogService.ShowMessage(ex.Message);
                           }
                       }));
            }
        }

        private RelayCommand _addCommand;

        public RelayCommand AddCommand
        {
            get
            {
                return _addCommand ?? (_addCommand = new RelayCommand(obj =>
                {
                    var phone = new Phone();
                    Phones.Insert(0, phone);
                    SelectedPhone = phone;
                }));

            }
        }

        private RelayCommand _removeCommand;

        public RelayCommand RemoveCommand
        {
            get
            {
                return _removeCommand ?? (_removeCommand = new RelayCommand(obj =>
                {
                    if (obj is Phone phone)
                    {
                        Phones.Remove(phone);
                    }
                }, obj => Phones.Count > 0));
            }
        }

        private RelayCommand _doubleCommand;
        public RelayCommand DoubleCommand
        {
            get
            {
                return _doubleCommand ??
                       (_doubleCommand = new RelayCommand(obj =>
                       {
                           if (!(obj is Phone phone)) return;
                           var phoneCopy = new Phone
                           {
                               Company = phone.Company,
                               Price = phone.Price,
                               Title = phone.Title
                           };
                           Phones.Insert(0, phoneCopy);
                       }));
            }
        }

        public Phone SelectedPhone
        {
            get => _selectedPhone;
            set
            {
                _selectedPhone = value;
                OnPropertyChanged("SelectedPhone");
            }
        }

        public ApplicationViewModel(IDialogService dialogService, IFileService fileService)
        {
            _dialogService = dialogService;
            _fileService = fileService;

            Phones = new ObservableCollection<Phone>
            {
                new Phone { Title="iPhone 7", Company="Apple", Price=56000 },
                new Phone {Title="Galaxy S7 Edge", Company="Samsung", Price =60000 },
                new Phone {Title="Elite x3", Company="HP", Price=56000 },
                new Phone {Title="Mi5S", Company="Xiaomi", Price=35000 }
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string prop = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
    }
}
