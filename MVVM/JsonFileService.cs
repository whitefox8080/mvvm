﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Json;

namespace MVVM
{
    public class JsonFileService : IFileService
    {
        public List<Phone> Open(string filename)
        {
            var phones = new List<Phone>();
            var jsonFormatter =
                new DataContractJsonSerializer(typeof(List<Phone>));
            using (var fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                phones = jsonFormatter.ReadObject(fs) as List<Phone>;
            }

            return new List<Phone>();
        }

        public void Save(string filename, List<Phone> phonesList)
        {
            var jsonFormatter =
                new DataContractJsonSerializer(typeof(List<Phone>));
            using (var fs = new FileStream(filename, FileMode.Create))
            {
                jsonFormatter.WriteObject(fs, phonesList);
            }
        }
    }
}
